package ru.tsc.tambovtsev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.dto.request.UserLoginRequest;
import ru.tsc.tambovtsev.tm.dto.request.UserLogoutRequest;
import ru.tsc.tambovtsev.tm.dto.request.UserProfileRequest;
import ru.tsc.tambovtsev.tm.dto.response.UserLoginResponse;
import ru.tsc.tambovtsev.tm.dto.response.UserLogoutResponse;
import ru.tsc.tambovtsev.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IAuthEndpoint extends IEndpoint{

    @NotNull
    String NAME = "AuthEndpoint";

    @NotNull
    String PART = NAME + SERVICE;

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IAuthEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IAuthEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    );

    @NotNull
    @WebMethod
    UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    );

    @NotNull
    @WebMethod
    UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    );

}
