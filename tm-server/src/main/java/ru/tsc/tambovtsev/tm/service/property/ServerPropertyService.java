package ru.tsc.tambovtsev.tm.service.property;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.service.property.IServerPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class ServerPropertyService implements IServerPropertyService {

    @Value("${environment['server.port']:8080}")
    private Integer serverPort;

    @Value("${environment['server.host']:localhost}")
    private String serverHost;

}
