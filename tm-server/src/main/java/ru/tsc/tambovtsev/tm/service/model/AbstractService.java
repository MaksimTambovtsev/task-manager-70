package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IGraphRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IService;
import ru.tsc.tambovtsev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.tambovtsev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public abstract class AbstractService<M extends AbstractEntity, R extends IGraphRepository<M>> implements IService<M> {

    @NotNull
    public abstract IGraphRepository<M> getRepository();

    @Nullable
    @Override
    @SneakyThrows
    public List<M> findAll() {
        @NotNull final IGraphRepository<M> repository = getRepository();
        return repository.findAll();
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<M> models) {
        if (models.isEmpty()) throw new TaskNotFoundException();
        @NotNull final IGraphRepository<M> repository = getRepository();
        repository.saveAll(models);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<M> set(@NotNull final Collection<M> models) {
        if (models.isEmpty()) throw new ProjectNotFoundException();
        @NotNull final IGraphRepository<M> repository = getRepository();
        repository.deleteAll();
        repository.saveAll(models);
        return models;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final M model) {
        Optional.ofNullable(model).orElseThrow(NullPointerException::new);
        @NotNull final IGraphRepository<M> repository = getRepository();
        repository.save(model);
    }

}
