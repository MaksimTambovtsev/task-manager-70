package ru.tsc.tambovtsev.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.ProjectClearRequest;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Remove all projects.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        System.out.println("[PROJECT CLEAR]");
        @Nullable final String userId = getUserId();
        getProjectEndpoint().clearProject(new ProjectClearRequest(getToken()));
    }

}

