package ru.tsc.tambovtsev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.tsc.tambovtsev.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/user")
public interface IUserEndpointImpl {

    @WebMethod
    @PostMapping("/save")
    User save(@NotNull @WebParam(name = "user") @RequestBody User user);

    @WebMethod
    @GetMapping("/findAll")
    List<User> findAll();

    @WebMethod
    @GetMapping("/findByLogin/{login}")
    User findByLogin(@NotNull @WebParam(name = "login") @PathVariable("login") String login);

    @WebMethod
    @PostMapping("/delete")
    void delete(@NotNull @WebParam(name = "user") @RequestBody User user);

    @WebMethod
    @DeleteMapping("/deleteByLogin/{login}")
    void deleteByLogin(@NotNull @WebParam(name = "login") @PathVariable("login") String login);

    @WebMethod
    @GetMapping("/count")
    long count();

}
