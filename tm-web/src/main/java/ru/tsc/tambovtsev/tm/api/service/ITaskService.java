package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    Task create(@Nullable String userId);

    @NotNull
    Task create(@Nullable String userId, @NotNull String name);

    @NotNull
    Task save(@NotNull Task task);

    @Nullable
    Collection<Task> findAll(@Nullable String userId);

    @Nullable
    Task findById(@Nullable String userId, @NotNull String id);

    void removeById(@Nullable String userId, @NotNull String id);

    void remove(@NotNull Task task);

    void remove(@NotNull List<Task> tasks);

    void removeByUserId(@Nullable final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@Nullable String userId, @NotNull String id);

    void clear(@Nullable String userId);

    long count(@Nullable String userId);

}
