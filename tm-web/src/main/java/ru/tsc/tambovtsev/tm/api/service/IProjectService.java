package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.Collection;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project create(@Nullable String userId);

    @NotNull
    Project create(@Nullable String userId, @NotNull String name);

    @NotNull
    Project save(@NotNull Project project);

    @Nullable
    Collection<Project> findAll(@Nullable String userId);

    @Nullable
    Project findById(@Nullable String userId, @NotNull String id);

    void removeById(@Nullable String userId, @NotNull String id);

    void remove(@NotNull Project project);

    void remove(@NotNull List<Project> projects);

    void removeByUserId(@Nullable final String userId, @NotNull final String id);

    boolean existsByUserIdAndId(@Nullable String userId, @NotNull String id);

    void clear(@Nullable String userId);

    long count(@Nullable String userId);

}
