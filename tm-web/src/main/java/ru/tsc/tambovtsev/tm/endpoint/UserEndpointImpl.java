package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.tambovtsev.tm.api.endpoint.IUserEndpointImpl;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.service.UserService;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/user")
public class UserEndpointImpl implements IUserEndpointImpl {

    @NotNull
    @Autowired
    private IUserService userService;

    @Override
    @PostMapping("/save")
    public User save(@NotNull @RequestBody final User user) {
        return userService.save(UserUtil.getUserId(), user);
    }

    @Override
    @GetMapping("/findAll")
    public List<User> findAll() {
        return userService
                .findAll(UserUtil.getUserId())
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findByLogin/{login}")
    public User findByLogin(@NotNull @PathVariable("login") final String login) {
        return userService.findByLogin(UserUtil.getUserId(), login);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final User user) {
        userService.remove(UserUtil.getUserId(), user);
    }

    @Override
    @DeleteMapping("/deleteByLogin/{login}")
    public void deleteByLogin(@NotNull String login) {
        userService.removeByLogin(UserUtil.getUserId(), login);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return userService.count(UserUtil.getUserId());
    }

}
