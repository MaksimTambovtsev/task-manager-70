package ru.tsc.tambovtsev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.tsc.tambovtsev.tm.api.endpoint.IProjectEndpointImpl;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/project")
public class ProjectEndpointImpl implements IProjectEndpointImpl {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService
                .findAll(UserUtil.getUserId())
                .stream()
                .collect(Collectors.toList());
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@NotNull @PathVariable("id") final String id) {
        return projectService.findById(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return projectService.existsByUserIdAndId(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping("/save")
    public Project save(@NotNull @RequestBody final Project project) {
        project.setUserId(UserUtil.getUserId());
        return projectService.save(project);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final Project project) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<Project> projects) {
        projectService.remove(projects);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        projectService.clear(UserUtil.getUserId());
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        projectService.removeById(UserUtil.getUserId(), id);
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return projectService.count(UserUtil.getUserId());
    }

}
