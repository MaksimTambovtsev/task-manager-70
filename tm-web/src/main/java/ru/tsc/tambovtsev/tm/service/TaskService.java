package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.repository.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.ITaskService;
import ru.tsc.tambovtsev.tm.exception.AccessException;
import ru.tsc.tambovtsev.tm.model.CustomUser;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    public Task create(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        @NotNull final Task task = new Task(
                "New Task: " + System.currentTimeMillis(),
                "Description",
                new Date(),
                new Date());
        task.setUserId(userId);
        save(task);
        return task;
    }

    @NotNull
    @Override
    public Task create(@Nullable final String userId, @NotNull final String name) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        @NotNull final Task task = new Task(
                name,
                "Description",
                new Date(),
                new Date());
        task.setUserId(userId);
        save(task);
        return task;
    }

    @NotNull
    @Override
    public Task save(@NotNull final Task task) {
        if (task.getUserId() == null || task.getUserId().isEmpty()) throw new AccessException();
        return repository.save(task);
    }

    @Nullable
    @Override
    public Collection<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.findByUserIdAndId(userId, id);
    }

    @Override
    public void removeById(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public void remove(@NotNull final Task task) {
        if (task.getUserId() == null || task.getUserId().isEmpty()) throw new AccessException();
        repository.delete(task);
    }

    @Override
    public void remove(@NotNull final List<Task> tasks) {
        tasks
                .stream()
                .forEach(this::remove);
    }

    @Override
    public void removeByUserId(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public boolean existsByUserIdAndId(@Nullable final String userId, @NotNull final String id) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.existsByUserIdAndId(userId, id);
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        repository.deleteAll();
    }

    @Override
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.count();
    }

}
