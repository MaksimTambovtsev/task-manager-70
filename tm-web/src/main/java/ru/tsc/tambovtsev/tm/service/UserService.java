package ru.tsc.tambovtsev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.IUserService;
import ru.tsc.tambovtsev.tm.exception.AccessException;
import ru.tsc.tambovtsev.tm.model.User;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.Collection;

@Service
public final class UserService implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @PreAuthorize("hasRole('ADMIN')")
    public User create(
            @Nullable final String userId,
            @Nullable String login,
            @Nullable String passwordHash,
            @Nullable String email
    ) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        @NotNull final User user = new User(
                login,
                passwordHash,
                email);
        save(userId, user);
        return user;
    }

    @NotNull
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public User save(@Nullable final String userId, @NotNull final User user) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.save(user);
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public Collection<User> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.findAll();
    }

    @Nullable
    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public User findByLogin(@Nullable final String userId, @NotNull final String login) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.findByLogin(login);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void removeByLogin(@Nullable final String userId, @NotNull final String login) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        repository.deleteByLogin(login);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public void remove(@Nullable final String userId, @NotNull final User user) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        repository.delete(user);
    }

    @Override
    @PreAuthorize("hasRole('ADMIN')")
    public long count(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new AccessException();
        return repository.count();
    }

}
