package ru.tsc.tambovtsev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.Collection;

public interface IUserService {

    @NotNull
    User save(@Nullable final String userId, @NotNull final User user);

    @Nullable
    Collection<User> findAll(@Nullable final String userId);

    @Nullable
    User findByLogin(@Nullable final String userId, @NotNull final String login);

    void removeByLogin(@Nullable final String userId, @NotNull final String login);

    void remove(@Nullable final String userId, @NotNull final User user);

    long count(@Nullable final String userId);

}
